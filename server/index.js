var express = require('express')
var app = express()
var router = express.Router()
var bodyParser = require('body-parser')
var fs = require('fs')
var cors = require('cors')

app.use(bodyParser.json())
// Add headers
app.use(function (req, res, next) {
  
      // Website you wish to allow to connect
      res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  
      // Request methods you wish to allow
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
      // Request headers you wish to allow
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  
      // Set to true if you need the website to include cookies in the requests sent
      // to the API (e.g. in case you use sessions)
      res.setHeader('Access-Control-Allow-Credentials', true);
  
      // Pass to next layer of middleware
      next();
});

app.get('/getForm/:formId', function(req, res) {
  fs.readFile('./database/'+req.params.formId+'.json', function(err, data) {
    if (err) throw err 
    res.write(data)
    res.end()
  })
})

app.post('/postForm/', function(req, res) {
  console.log("Request received!")
  fs.appendFile('./database/'+req.body.formUid+'.json', JSON.stringify(req.body), function(err){
    if (err)  throw err
  })
})

app.get('/formList/', function(req, res) {
  fs.readdir('./database', function(err, data) {
    if (err) throw err
    else {
      res.json(data.map(e=> e.slice(-e.length, -5)))
      res.end()
    }
  })
})

app.listen(8080, function() {
  console.log("App started!")
})
