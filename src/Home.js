import React, { Component } from 'react'
import Paper from 'material-ui/Paper'

export default class Home extends Component {
	render(){
		return(
			<Paper zDepth={3} style={styles.main}>
				<h2>Welcome to the Form App</h2>
      </Paper>
		)
	}
}

const styles = {
  main: {
    height: '80vh',
    width: '80vw',
    margin: '6vh',
    textAlign: 'center',
    display: 'flex',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  }
};