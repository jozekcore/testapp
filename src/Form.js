import React, { Component } from 'react'
import axios from 'axios'
import Checkbox from 'material-ui/Checkbox'
import RaisedButton from 'material-ui/RaisedButton'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton'

export default class ViewForm extends Component {
	constructor(props) {
		super(props)
		this.state = {
			idForm: this.props.idForm,
			data: {},
			answer: null
		}
	}
  componentWillMount(){
	  let context = this
    let instance = axios.create({
			baseURL: 'http://localhost:8080',
			headers: {
				'Content-Type': 'application/json'
			}
		})

		instance.get('/getForm/'+context.state.idForm)
		.then(function (response) {
			console.log(response)
			let dataWithoutAnswers = JSON.parse(JSON.stringify(response.data))
			dataWithoutAnswers.questions.map(question=> {
				question.isCorrect = null
				question.answers.map(answer => {
					answer.correct = false
				})
			})
			context.setState({ data:{...response.data}, answer: {...dataWithoutAnswers} })
		})
		.catch(function (error) {
			console.log(error);
		});
	}

		updateCheck = (id, checked)=> {
			let answer = {...this.state.answer}
			answer.questions.map(question => {
				question.answers.map(newAnswer => {
					if(newAnswer.id === id) {
						newAnswer.correct = checked
						this.setState({answer})
					}
				})
			})
		}
	
		updateRadio = (id, value)=> {
			let answer = {...this.state.answer}
			answer.questions.map(question => {
				question.answers.map(newAnswer => {
					if(newAnswer.id === id) {
						newAnswer.correct = value
						this.setState({answer})
					} else {
						newAnswer.correct = false
						this.setState({answer})
					}
				})
			})
		}

		checkCorrect = (idCheck) => {
			let answer = {...this.state.answer}
			
			for(let a in answer.questions){
				console.log(answer.questions[a].idQuestion, 'answer')
				console.log(idCheck,'idCheck')
				console.log(answer.questions[a].idQuestion == idCheck, 'answer')
				
				if(answer.questions[a].idQuestion == idCheck ){
					for(let b in answer.questions[a].answers){
						if(answer.questions[a].answers[b].correct !== this.state.data.questions[a].answers[b].correct){
							answer.questions[a].isCorrect = false
							this.setState({answer})
							break
						} else {
							answer.questions[a].isCorrect = true
							this.setState({answer})
						}
					}
				}
			}
		}
	
		render(){
			return(
			<div style={styles.container}>
				{this.state.answer &&
					this.state.answer.questions.map(question => question.type == 'selection' ? 
							(
								<div style={styles.question}>
									<h3>{question.title}</h3>
									<h4>> {question.subtitle}</h4>
									<div style={styles.answerRadio}>									
										<RadioButtonGroup name="shipSpeed" onChange={(e, value)=> this.updateRadio(e.target.id, value)} >
											{
												question.answers.map(answer => 
													<RadioButton
														id={answer.id} 
														value={answer.id} 
														label={answer.title}
														style={styles.radioButton}/>
												)
											}
										</RadioButtonGroup>
									</div>
									{
										question.isCorrect !== null ? ( question.isCorrect === true ? 
												<p style={{color: 'green'}}> is correct! </p> : 
												<p style={{color: 'red'}}> is incorrect! </p> ) : null
									}
									<RaisedButton onClick={(e)=> {let a = question.idQuestion; this.checkCorrect(a)}} label="Submit" primary style={styles.button} />
								</div>
							) 
							:
							(
								<div style={styles.question}>
									<h3>{question.title}</h3>
									<h4>> {question.subtitle}</h4>
									<div style={styles.answerCheck}>
										{
											question.answers.map(answer=> 
												<Checkbox
													id={answer.id}
													label={answer.title}
													checked={answer.correct}
													style={styles.checkbox}
													onCheck={(e, checked)=> this.updateCheck(e.target.id, checked)}/>
											)
										}
									</div>
									{
										question.isCorrect !== null ? ( question.isCorrect === true ? 
												<p style={{color: 'green'}}> is correct! </p> : 
												<p style={{color: 'red'}}> is incorrect! </p> ) : null
									}
									<RaisedButton onClick={(e)=> {let a = question.idQuestion; this.checkCorrect(a)}} label="Submit" primary style={styles.button} />
								</div>
							)
						)
				}
			</div>
		)
	}
}

const styles = {
  main: {
    height: '80vh',
    width: '80vw',
    margin: '6vh',
    textAlign: 'center',
    display: 'flex',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
	}, 
	container: {
		display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center'
	}, 
	question: {
		padding: '5vh'		
	}, 
	button: {
		marginTop: '2vh'
	}
}