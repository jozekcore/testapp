import React, { Component } from 'react'
import Paper from 'material-ui/Paper'
import axios from 'axios'
import Form from './Form.js'

export default class ViewForm extends Component {
  constructor(){
    super()
    this.state = {
      formID: undefined,
      json: []
    }
  }

  componentWillMount(){
    let context = this
    let instance = axios.create({
			baseURL: 'http://localhost:8080',
			headers: {
				'Content-Type': 'application/json'
			}
		})

		instance.get('/formList/')
		.then(function (response) {
      console.log(response)
			context.setState({ json: response.data})
		})
		.catch(function (error) {
			console.log(error);
		});
  }

  onClickForm = (id)=>{
    this.setState({
      formID: id
    })
  }

	render(){
		return(
			<Paper zDepth={3} style={styles.main}>
        {
          this.state.formID == undefined ? 
          <div>  
            <h2>Choose your form</h2>
              {
                this.state.json.map((formid, index) => <h2 id={formid} onClick={(e)=> this.onClickForm(e.target.id)} style={styles.form}> Form {index}</h2>)
              } 
          </div> : 
          <Form idForm={this.state.formID}/>
        } 
      </Paper>
		)
	}
}

const styles = {
  main: {
    height: '100%',
    width: '80vw',
    margin: '6vh',
    padding: '5vh',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
  form: {
    cursor: 'pointer'
  }
}