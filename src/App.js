import React, { Component } from 'react'
import './App.css'
import Home from './Home.js'
import ViewForm from './ViewForm.js'
import CreateForm from './CreateForm.js'
import FlatButton from 'material-ui/FlatButton'

class App extends Component {
  constructor(){
    super()
    this.state = {
      option: 1
    }
  }

 renderOption = ()=> {
  switch (this.state.option) {
    case 1: 
      return <Home />
    break
    case 2:
      return <ViewForm />
    break
    case 3:
      return <CreateForm />
    break
  }
}

  render() {
    return (
      <div className="App">
        <div className="topNav">
          <FlatButton style={styles.button} onClick={()=> this.setState({option: 1})} label="Test App" />
          <FlatButton style={styles.button} onClick={()=> this.setState({option: 2})} label="View"/>
          <FlatButton style={styles.button} onClick={()=> this.setState({option: 3})} label="Create"/>
        </div>
          {this.renderOption()}
      </div>
    );
  }
}

const styles = {
  button: {
    color: '#fff'
  }
};

export default App;
