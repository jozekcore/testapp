import React, { Component } from 'react'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import {Tabs, Tab} from 'material-ui/Tabs'
import Checkbox from 'material-ui/Checkbox'
import FlatButton from 'material-ui/FlatButton'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton'
import uuid from 'uuid'
import axios from 'axios'

export default class CreateForm extends Component {
	constructor(props){
		super(props)
		this.state = {
			question: {
				idQuestion: uuid(),
				title: "",
				subtitle: "",
				type: "selection",
				answers: [{
						id: uuid(),
						title: "",
						correct: false
					}, 
					{
						id: uuid(),
						title: "",
						correct: false
					},
					{
						id: uuid(),
						title: "",
						correct: false 
				}]
		},
		json: {
			formUid: uuid(),
			questions: []
		}
	}
}

	 handleTabChange = (value) => {
			let question = {...this.state.question}
			question.type = value
			question.answers.map(answer => answer.correct = false)
			this.setState({question})
	  }

	updateCheck = (id, checked)=> {
		let question = {...this.state.question}
		question.answers.map(answer => {
			if(answer.id === id) {
				answer.correct = checked
				this.setState({question})
				console.log(this.state.question)
			}
		})
	}

	updateRadio = (id, value)=> {
		console.log(id, value)
		let question = {...this.state.question}
		question.answers.map(answer => {
			if(answer.id === id) {
				answer.correct = value
				this.setState({question})
				console.log(this.state.question)
			} else {
				answer.correct = false
				this.setState({question})
				console.log(this.state.question)
			}
		})
	}
	
	onAddAnswer = ()=> {
		let question = {...this.state.question}
		question.answers.push({id: uuid(), title: '', correct: false})
		this.setState({question})
	}

	onChangeAnswer= (e, text) => {
		let question = {...this.state.question}
		question.answers.map(answer => {
			if(answer.id === e) {
				answer.title = text
				this.setState({question})
			}
		})
	}

	onChangeTitle= (e, text) => {
		let question = {...this.state.question}
		question.title = text
		this.setState({question})
	}

	onChangeSubtitle= (e, text) => {
		let question = {...this.state.question}
		question.subtitle = text
		this.setState({question})
	}

	onNewQuestion = ()=> {
		let newState = {...this.state}
		newState.json.questions.push(newState.question)
		newState.question = {
			idQuestion: uuid(),
			title: "",
			subtitle: "",
			type: "selection",
			answers: [{
					id: uuid(),
					title: "",
					correct: false
				}, 
				{
					id: uuid(),
					title: "",
					correct: false
				},
				{
					id: uuid(),
					title: "",
					correct: false 
			}]
		}
		this.setState(newState)
		console.log(this.state)
	}

	submitForm = ()=> {
		let instance = axios.create({
			baseURL: 'http://localhost:8080',
			headers: {
				'Content-Type': 'application/json'
			}
		})

		instance.post('/postForm/', {
			...this.state.json
		})
		.then(function (response) {
			console.log(response);
		})
		.catch(function (error) {
			console.log(error);
		});
	}

	render(){
		return(
			<Paper zDepth={3} style={styles.main}>
				<h3>CREATE FORM</h3>
				<Tabs value={this.state.question.type} onChange={this.handleTabChange}>
	        <Tab label="Multiple selection question" style={styles.tab} value="selection">
						<TextField id="title" hintText="Question title" onChange={(e, text)=> this.onChangeTitle(e.target.id, text)} value={this.state.question.title} fullWidth/>
						<TextField id="subtitle" hintText="Question subtitle" onChange={(e, text)=> this.onChangeSubtitle(e.target.id, text)} value={this.state.question.subtitle} fullWidth/>
						<div style={styles.radiocontainer}>
							<div style={styles.divradio1}>
								{	
									this.state.question.answers.map(answer => 
										<TextField 
											id={answer.id} 
											hintText="Answer" 
											value={answer.title} 
											onChange={(e, text)=> this.onChangeAnswer(e.target.id, text)} 
											fullWidth/>
									)
								}
							</div>							
							<div style={styles.divradio2}>
								<RadioButtonGroup style={styles.divradio3} onChange={(e, value)=> this.updateRadio(e.target.id, value)} name="shipSpeed">
									{	
										this.state.question.answers.map((answer, index) => 
											<RadioButton 
												id={answer.id} 
												value={answer.id} 
												label="Correct?" 
												style={styles.radioButton}/> 
										)
									}
								</RadioButtonGroup>
							</div>							
						</div>
						<FlatButton onClick={()=> this.onAddAnswer()} label="Add new answer" primary style={styles.button} />
	        </Tab>
	        
					<Tab label="Multiple choice question" value="choice">
	          <TextField hintText="Question title" onChange={(e, text)=> this.onChangeTitle(e.target.id, text)} value={this.state.question.title} fullWidth/>
						<TextField hintText="Question subtitle" onChange={(e, text)=> this.onChangeSubtitle(e.target.id, text)} value={this.state.question.subtitle} fullWidth/>
						{	
							this.state.question.answers.map(answer => {
								return (
									<div style={styles.divcheck}>
										<TextField id={answer.id} hintText="Answer" value={answer.title} onChange={(e, text)=> this.onChangeAnswer(e.target.id, text)} fullWidth/>
										<Checkbox
											id={answer.id}
											label="Correct?"
											checked={answer.correct}
											onCheck={(e, checked)=> this.updateCheck(e.target.id, checked)}
											style={styles.checkbox}/>
									</div>
									)		
							})
						}
						 <FlatButton onClick={()=> this.onAddAnswer()} label="Add new answer" primary style={styles.button} />
	        </Tab>
	      </Tabs>
				<div style={styles.bottomButtons}>
					<RaisedButton label="Add question" onClick={()=> this.onNewQuestion()} primary style={styles.button} />
					<RaisedButton label="SUBMIT FORM" onClick={()=> this.submitForm()} primary style={styles.button} />				
				</div>
      </Paper>
		)
	}
}

const styles = {
  main: {
    height: '80vh',
    width: '80vw',
    margin: '6vh',
    textAlign: 'center',
    display: 'flex',
    alignSelf: 'center',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#EEEEEE'
  },
  tab: {
		width: '40vw'
  },
  button: {
  	alignSelf: 'center'
  },
  divcheck:{
  	display: 'flex',
  	flexDirection: 'row',
  	alignItems: 'center'
	},
	radiocontainer:{
  	display: 'flex',
  	flexDirection: 'row',
  	alignItems: 'center'
	},
	divradio1: {
		display: 'flex',
  	flexDirection: 'column',
		alignItems: 'center',
		width:'50%',
		height: '100%'
	},
	divradio3: {
		display: 'flex',
    justifyContent: 'space-around',
		flexDirection: 'column',
		height: '100%'
	},
	divradio2: {
		display: 'flex',
    justifyContent: 'space-around',
		flexDirection: 'column',
		height: '100%',
	},
	radioButton: {
		cursor: 'pointer',
    position: 'relative',
    overflow: 'visible',
    display: 'flex',
    height: '5vh',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '1vh'
	},
	bottomButtons: {
		display: 'flex',
		flexDirection: 'row',
		width: '45%',
    justifyContent: 'space-around'
	}
};