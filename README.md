# Google Forms simulator app

A test application in base of [Google Forms](https://www.google.com/forms)

## Getting Started

Your first need to clone or download the project to run it. 

```
mkdir your_folder
cd your_folder
git clone https://gitlab.com/jozekcore/testapp.git
```

### Prerequisites

*Node
*Npm or Yarn

### Installing

This is a step by step series of examples that tell you have to get a development env running

 * First Step

```
Open a terminal, go to the folder and write

npm install
```

* Second Step

```
node server/
```

* Third Step

```
Open a new terminal, go to the folder and write

npm start
```

And that's it, your running the app.


## Built With

* [React Js](https://reactjs.org/) - The front-end library used
* [Material-Ui](www.material-ui.com) - Material design components
* [Express](http://expressjs.com/) - Services and API'S
* [Node](https://nodejs.org/en/) - Server

## Live Preview

* [Live Preview](https://redes1-jdbarrios.c9users.io/testapp/build/index.html) - Live Preview Link

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details